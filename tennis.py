class ScoreKeeper:
    def _init_(self):
        self.points = {'A': 0, 'B': 0}
        self.games = {'A': 0, 'B': 0}
        self.sets = {'A': 0, 'B': 0}
        self.point_values = [0, 15, 30, 40]

    def update_points(self, player):
        if self.points[player] < 3: 
            self.points[player] += 1
        else: 
            self.points[player] = 0  
            self.update_games(player)

    def update_games(self, player):
        self.games[player] += 1
        if self.games[player] == 6:  
            self.games[player] = 0 
            self.update_sets(player)

    def update_sets(self, player):
        self.sets[player] += 1

    def point_won_by(self, player):
        self.update_points(player)

    def winner(self):
        if self.sets['A'] == 5:
            return "A"
        elif self.sets['B'] == 5:
            return "B"
        return None

    def score(self):
        return {
            'points': {player: self.point_values[self.points[player]] for player in self.points},
            'games': self.games,
            'sets': self.sets
        }

def simulate_game(input_string):
    game = ScoreKeeper()
    for char in input_string:
        game.point_won_by(char)
        winner = game.winner()
        if winner:
            return f"Winner: Player {winner}"
    return game.score()


input_string = "AABB"  
result = simulate_game(input_string)
print(result)
